<?php

class PhotosController extends BaseController
{

    /**
     * Photo Repository
     *
     * @var Photo
     */
    protected $photo;


    public function __construct(Photo $photo)
    {
//        $this->beforeFilter(function($e)
//        {
//            $album = Album::findOrFail($e->parameter('album_id'));
//            if ($album->uid !== Auth::user()->uid) {
//                return 'You haven\'t access';
//            }
//
//        });
        $this->photo = $photo;
    }

    /**
     * Display a listing of the photos.
     * @param  album_id
     * @return Response
     */
    public function index($album_id)
    {
        $album = Album::where('id', '=', $album_id)->first();
        if (is_null($album)) {
            return 'Album not exsist';
        }

        $creator = $album->users()->where('user_id', '=', Auth::user()->id)->first();
        if (is_null($creator) || $creator->pivot->access < 1) {
            return 'You can\'t see this album';
        }

        $photos = $album->photos()->get();

        $album_name_list = Auth::user()->ownAlbums->lists('name', 'id');

        return View::make('photos.index', compact('photos', 'album_id', 'creator', 'album_name_list'));
    }

    /**
     * Show the form for adding new photos.
     * @var $album_id
     * @return Response
     */
    public function create($album_id)
    {
        $album = Album::where('id', '=', $album_id)->first();
        if (is_null($album)) {
            return 'Album not exist';
        }

        $creator = $album->users()->where('user_id', '=', Auth::user()->id)->first();
        if (is_null($creator) || $creator->pivot->access <= 1) {
            return 'You can\'t add photo to this album';
        }
        return View::make('photos.create', compact('album_id'));
    }

    /**
     * Store a newly created resource in storage.
     * @var $album_id
     * @return Response
     */
    public function store($album_id)
    {
        $album = Album::where('id', '=', $album_id)->first();
        if (is_null($album)) {
            return 'Album not exist';
        }

        $creator = $album->users()->where('user_id', '=', Auth::user()->id)->first();
        if (is_null($creator) || $creator->pivot->access <= 1) {
            return 'You can\'t add photo to this album';
        }

        $input = Input::all();
        $validation = Validator::make($input, Photo::$rules);

        if ($validation->passes()) {
            $img = Image::make(Input::file('src'));

            $dir = public_path() . '/files/' . $album_id;

            $image_size = Input::file('src')->getSize();
            $image_name = Input::file('src')->getClientOriginalName();
            @mkdir($dir);
//            print_r(Input::file('src')->getClientOriginalExtension());
//            print_r(Input::file('src')->getSize());

            $img->save($dir . '/' . $image_name);
            $img->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save($dir . '/thumb_' . $image_name);

            $input['src'] = $image_name;
            $input['size'] = $image_size;
            $album->photos()->create($input);

            return Redirect::action('PhotosController@index', [$album_id]);
        }

        return Redirect::route('photos.create')
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @param  int $album_id
     * @return Response
     */
    public function show($album_id, $id)
    {
        $album = Album::where('id', '=', $album_id)->first();
        if (is_null($album)) {
            return 'Album not exist';
        }

        $creator = $album->users()->where('user_id', '=', Auth::user()->id)->first();
        if (is_null($creator) || $creator->pivot->access <= 1) {
            return 'You can\'t see photo from this album';
        }

        $photo = $this->photo->findOrFail($id);

        return View::make('photos.show', compact('photo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $photo = $this->photo->find($id);

        if (is_null($photo)) {
            return Redirect::route('photos.index');
        }

        return View::make('photos.edit', compact('photo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $input = array_except(Input::all(), '_method');
        $validation = Validator::make($input, Photo::$rules);

        if ($validation->passes()) {
            $photo = $this->photo->find($id);
            $photo->update($input);

            return Redirect::route('photos.show', $id);
        }

        return Redirect::route('photos.edit', $id)
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');
    }

    public function move($album_id, $id)
    {
        $from_album = Album::find($album_id);
        $creator = $from_album->users()->where('user_id', '=', Auth::user()->id)->first();

        if ($creator->pivot->access <= 1) {
            return 'You can\'t move from this album';
        }

        $to_id = Input::get('id');

        $to_album = Album::find($to_id);
        $creator = $to_album->users()->where('user_id', '=', Auth::user()->id)->first();

        if ($creator->pivot->access <= 1) {
            return 'You can\'t move to this album';
        }

        $photo = $this->photo->findOrFail($id);

        $oldfile = public_path() . '/files/'.$album_id.'/'.$photo->src;
        $newfile = public_path() . '/files/'.$to_id.'/'.$photo->src;
        File::move($oldfile, $newfile);
        $oldfile = public_path() . '/files/'.$album_id.'/thumb_'.$photo->src;
        $newfile = public_path() . '/files/'.$to_id.'/thumb_'.$photo->src;
        File::move($oldfile, $newfile);
        $photo->album_id = $to_id;
        $photo->update();
        return 'Ok, moved to' . $to_id;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @param  int $album_id
     * @return Response
     */
    public function destroy($album_id, $id)
    {
        $album = Album::where('id', '=', $album_id)->first();
        if (is_null($album)) {
            return 'Album not exist';
        }

        $creator = $album->users()->where('user_id', '=', Auth::user()->id)->first();
        if (is_null($creator) || $creator->pivot->access <= 1) {
            return 'You can\'t delete photo from this album';
        }

        $photo = $this->photo->find($id);

        $photo->delete();

        return Redirect::action('PhotosController@index', [$album_id]);;
    }

}
