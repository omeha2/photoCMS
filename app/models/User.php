<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;


	public function ownAlbums(){
		return $this->belongsToMany('Album')->where('access','=','3')->withPivot('access');
	}

	/* 0 - Haven't access
	 * 1 - Can read
	 * 2 - Can write
	 * 3 - Creator
	 * */
//	public function albumAccess($album_id){
//		$a = $this->belongsToMany('Album')->where('album_id','=',$album_id)->withPivot('access');
//		if($a->count() == 0 || $a->first()->pivot->access == ''){
//			return '0';
//		}
//		return ($a->first()->pivot->access);
//	}
	public function album()
	{
		return $this->belongsToMany('Album')->withPivot('access');
	}

	public function shared(){
		return $this->belongsToMany('Album')->where('access','<','3')->withPivot('access');
	}
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');
	protected $fillable = array('id','login','password');

}
