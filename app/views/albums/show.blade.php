@extends('layouts.scaffold')

@section('main')

<h1>Show Album</h1>

<p>{{ link_to_route('albums.index', 'Return to all albums') }}</p>

<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>Uid</th>
				<th>Name</th>
				<th>Status</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $album->uid }}}</td>
					<td>{{{ $album->name }}}</td>
					<td>{{{ $album->status }}}</td>
                    <td>{{ link_to_route('albums.edit', 'Edit', array($album->id), array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('albums.destroy', $album->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
		</tr>
	</tbody>
</table>

@stop
