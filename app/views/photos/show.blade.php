@extends('layouts.scaffold')

@section('main')

<h1>Show Photo</h1>

{{--<p>{{ link_to_action('PhotosController@create', 'Add new photo',[$album_id]) }}</p>--}}
<p>{{ link_to_action('PhotosController@index', 'Return to all photos',[$photo->album_id]) }}</p>

<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>Photo</th>
		</tr>
	</thead>

	<tbody>
		<tr>
					<td>{{ HTML::image('/files/'.$photo->album_id.'/'. $photo->src,'width',['width'=>'100%']) }}</td>
{{--                    <td>{{ link_to_route('photos.edit', 'Edit', array($photo->id), array('class' => 'btn btn-info')) }}</td>--}}
                    {{--<td>--}}
{{--                        {{ Form::open(array('method' => 'DELETE', 'route' => array('photos.destroy', $photo->id))) }}--}}
{{--                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}--}}
                        {{--{{ Form::close() }}--}}
                    {{--</td>--}}
		</tr>
	</tbody>
</table>

@stop
