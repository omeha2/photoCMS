@extends('layouts.scaffold')

@section('main')

<h1>All Photos</h1>
@if($creator->pivot->access>=2)
<p>{{ link_to_action('PhotosController@create', 'Add new photo',[$album_id]) }}</p>
@endif

@if ($photos->count())
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>Check</th>
				<th>Album id</th>
				<th>id</th>
				<th>Size</th>
				<th>Preview</th>
				<th>Actions</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($photos as $photo)
				<tr>
					<td>{{Form::checkbox('photos',$photo->id)}}</td>
					<td>{{{ $photo->album_id }}}</td>
					<td>{{{ $photo->id }}}</td>
					<td>{{{ $photo->size }}} byte</td>
					<td><a href="/albums/{{$photo->album_id}}/{{$photo->id}}/">{{ HTML::image('/files/'.$photo->album_id.'/thumb_'. $photo->src,'width',['width'=>'80px']) }}</a></td>
					@if($creator->pivot->access>=0)
                    <td>{{ link_to_action('PhotosController@edit', 'Edit', array($album_id,$photo->id), array('class' => 'btn btn-info','style'=>'float: left')) }}
                        {{ Form::open(array('method' => 'DELETE', 'action' => array('PhotosController@destroy', $album_id,$photo->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                        {{ Form::open(array('method' => 'POST', 'action' => array('PhotosController@move', $album_id,$photo->id))) }}
                            {{ Form::select('id', $album_name_list); }}
                            {{ Form::submit('Move', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
                    @endif
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no photos
@endif

@stop
